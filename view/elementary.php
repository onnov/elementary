<!DOCTYPE html>
<html>
    <head>
        <title>Elementary</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="/public/css/elementary.css">
    </head>
    <body>
        <table id="myTab">
            <tr style="height: 50px;">
                <td style="width: 100px; background-color: #000;"></td>
                <td id="topBlock" style="background-color: blue; vertical-align: middle;">
                    <!--<div class="myBlock">-->
                    <table style="display: inline-block;">
                        <tr style="color: orange; text-align: center;">
                            <td colspan="2">Rotation</td>
                        </tr>
                        <tr>
                            <td><a id="rotationLeft" class="icon-undo"></a></td>
                            <td><a id="rotationRight" class="icon-redo"></a></td>
                        </tr>
                    </table>

                    <table style="display: inline-block;">
                        <tr style="color: orange; text-align: center;">
                            <td colspan="2">Scale: <b id="scaleInfo">100%</b></td>
                        </tr>
                        <tr>
                            <td><a id="scaleMinus" class="icon-minus"></a></td>
                            <td><a id="scalePlus" class="icon-plus"></a></td>
                        </tr>
                    </table>

                    <table style="display: inline-block;">
                        <tr style="color: orange; text-align: center;">
                            <td colspan="2">Moving</td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if ($buttonBack != '')
                                {
                                    ?>
                                    <a class="icon-arrow-left2" href="<?= $buttonBack ?>"></a>
                                    <?php
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($buttonNext != '')
                                {
                                    ?>
                                    <a class="icon-arrow-right2" href="<?= $buttonNext ?>"></a>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                    </table>
                    
                    <audio id="audioPlayer" style="display: none; width: 600px;" controls autoplay src=""></audio>
                    
                    <?= $topLine ?>
                    <!--</div>-->
                </td>
            </tr>
            <tr>
                <td style="background-color: orange;">
                    <div id="leftMenu" class="myBlock">
                        <?= $leftMenu ?>
                    </div>
                </td>
                <td>
                    <div class="myBlock" style="text-align: center;">
                        <img id="imgPage" src="<?= $img ?>" alt="">
                    </div>
                </td>
            </tr>
        </table>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('img#imgPage').on('load', function () {
                    var img = $('img#imgPage');
                    var imgWidth = img.width();
                    //localStorage.removeItem('elementary');
                    if (typeof localStorage.elementary !== "undefined")
                    {
                        var set = JSON.parse(localStorage.elementary);
                        imgUpdate();

                    } else {
                        var set = {
                            'scale': 100,
                            'deg': 0,
                            'scrollLeftMenu': 0
                        };
                        localStorage.elementary = JSON.stringify(set);
                    }

                    if (set.scrollLeftMenu > 0)
                    {
                        $("#leftMenu").animate({
                            scrollTop: set.scrollLeftMenu
                        }, 500);
                    }

                    $("#leftMenu").scroll(function () {
                        localStorageUpdate({'scrollLeftMenu': $(this).scrollTop()});
                        //console.log($(this).scrollTop());
                    });

                    $('#rotationLeft').on('click', function () {
                        set.deg -= 90;
                        imgUpdate();
                    });
                    $('#rotationRight').on('click', function () {
                        set.deg += 90;
                        imgUpdate();
                    });
                    $('#scaleMinus').on('click', function () {
                        set.scale -= 10;
                        if (set.scale < 0)
                        {
                            set.scale = 0;

                        }
                        $('#scaleInfo').text(set.scale + '%');

                        imgUpdate();
                    });
                    $('#scalePlus').on('click', function () {
                        //imgWidth += pm;
                        set.scale += 10;
                        $('#scaleInfo').text(set.scale + '%');

                        imgUpdate();
                    });

                    function imgUpdate()
                    {
                        localStorage.elementary = JSON.stringify(set);
                        var w = imgWidth / 100 * set.scale;
                        img
                                .css('transform', 'rotate(' + set.deg + 'deg)')
                                .css('width', w + 'px');
                    }

                    function localStorageUpdate(obj)
                    {
                        var s = JSON.parse(localStorage.elementary);
                        for (key in obj)
                        {
                            s[key] = obj[key];
                        }
                        localStorage.elementary = JSON.stringify(s);
                    }
                });
                
                $('.aAudio').on('click', function() {
                    console.log($(this).data('file'));
                    $('#audioPlayer').css('display','inline-block');
                    $('#audioPlayer').attr('src', $(this).data('file'));
                });
            });
        </script>
    </body>
</html>


