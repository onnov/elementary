<?php

ini_set('display_errors', 'On');
error_reporting('E_ALL');

$get = $_GET;

$path  = 'public/elementary/img/';
$ext   = '.jpg';
$path2 = 'public/elementary/audio/';
$ext2  = '.mp3';

$topLine  = '';
$leftMenu = '';

$buttonBack = '';
$buttonNext = '';

function nameFile($path, $res, $ext)
{
    return str_replace($ext, '', str_replace($path, '', $res));
}

// обработка GET
if (isset($get['p']) && file_exists($path . $get['p'] . $ext))
{
    $p = $get['p'];
}
else
{
    $p = '0001';
}

$img = '/' . $path . $p . $ext;

// Список аудио файлов
$_audio = glob($path2 . '[0-9]*' . $ext2);

$audio = [];
foreach ($_audio as $res)
{
    $audio[] = nameFile($path2, $res, $ext2); //str_replace($ext2, '', str_replace($path2, '', $res));
}

// Список Страниц о учебнике
$_p = glob($path . '[I,V]*' . $ext);

// +Список страниц учебника
$result = array_merge($_p, glob($path . '[0-9]*' . $ext));
$iMax   = (count($result) - 1);
foreach ($result as $i => $res)
{
    $n = nameFile($path, $res, $ext); //str_replace($ext, '', str_replace($path, '', $res));

    $menuActiv = '';
    if ($n == $p)
    {
        $menuActiv = 'class="menuActiv"';

        if ($i == 0)
        {
            $back = '';
            $next = '/?p=' . nameFile($path, $result[($i + 1)], $ext);
        }
        else if ($i == $iMax)
        {
            $back = '/?p=' . nameFile($path, $result[($i - 1)], $ext);
            $next = '';
        }
        else
        {
            $back = '/?p=' . nameFile($path, $result[($i - 1)], $ext);
            $next = '/?p=' . nameFile($path, $result[($i + 1)], $ext);
        }

        $buttonBack = $back;
        $buttonNext = $next;
    }

    $aud = '';
    if (in_array($n, $audio))
    {
        $aud = '<a class="aAudio icon-volume-medium" data-file="/' . $path2 . $n . $ext2 . '"></a>';
    }

    $leftMenu .= '<div><a class="aPage" ' . $menuActiv . ' href="/?p=' . $n . '">' . $n . '</a>' . $aud . '</div>';
}

$leftMenu .= '<br><br><br>';



require 'view/elementary.php';
